<?php

use Roots\Sage\Setup;
use Roots\Sage\Wrapper;

?>

<?php if (is_category(Setup\get_main_categories())) : ?>
  <?php
    $do_not_duplicate = [];
    $category_obj = get_queried_object();
    $sections = get_field('sections', $category_obj);

    if ($sections) {
      foreach ($sections as $section) {
        $category_id = $category_obj->term_id;
        if ($section['acf_fc_layout'] === 'carousel') {
          $carousel_number_of_stories = $section['number_of_stories'];
          get_template_part('templates/category', 'featured-posts');
        } elseif ($section['acf_fc_layout'] === 'top_sections') {
          $top_section_categories = $section['categories'];
          get_template_part('templates/home', 'top-sections');
        } elseif ($section['acf_fc_layout'] === 'donate_section') {
          get_template_part('templates/home', 'donate');
        } elseif ($section['acf_fc_layout'] === 'featured_data_viz') {
          get_template_part('templates/home', 'special');
        } elseif ($section['acf_fc_layout'] === 'latest_stories') {
          get_template_part('templates/category', 'latest');
        }
      }
    }
  ?>
<?php else : ?>

  <?php while (have_posts()) : the_post(); ?>
    <article class="row mb-5">
      <div class="col-3">
        <?php if (has_post_thumbnail()) : ?>
          <?php
            $id = get_post_thumbnail_id();
            $alt = get_post_meta($id, '_wp_attachment_image_alt', true);

            $featured_image_latest_src = wp_get_attachment_image_src($id, 'indiaspend-post-featured-image-latest');
            $featured_image_latest_srcset = wp_get_attachment_image_srcset($id, 'indiaspend-post-featured-image-latest');
            $featured_image_latest_small_src = wp_get_attachment_image_src($id, 'indiaspend-post-featured-image-small');
            $featured_image_latest_small_srcset = wp_get_attachment_image_srcset($id, 'indiaspend-post-featured-image-small');
          ?>
          <a href="<?php the_permalink(); ?>">
            <img class="img-fluid"
                 src="<?php echo esc_attr($featured_image_latest_small_src[0]); ?>"
                 srcset="<?php echo esc_attr($featured_image_latest_small_srcset); ?>"
                 sizes="(min-width: 768px) 240px,
                        (min-width: 576px) 180px,
                        191px"
                 alt="<?php echo esc_attr($alt);?>">
          </a>
        <?php endif; ?>
      </div>
      <header class="col-9">
        <h3 class="entry-title" class="h4">
          <a href="<?php the_permalink(); ?>">
            <?php the_title(); ?>
          </a>
        </h3>
        <?php get_template_part('templates/entry-meta'); ?>
        <p class="d-none d-sm-block">
          <?= get_the_excerpt(); ?>
        </p>
      </header>
    </article>
  <?php endwhile; ?>

<?php endif; ?>
