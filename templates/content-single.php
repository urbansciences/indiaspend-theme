<?php while (have_posts()) : the_post(); ?>
  <?php echo do_shortcode('[ajax_load_more post_type="post" repeater="default" previous_post="true" previous_post_id="'. get_the_ID() .'" posts_per_page="1" button_label="Previous Post"]'); ?>
<?php endwhile; ?>
