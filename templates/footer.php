<footer class="content-info">
  <div class="container">
    <div class="row">
      <?php dynamic_sidebar('sidebar-footer'); ?>
      <section class="widget footer-social align-self-center">
        <a href="/subscribe" class="d-block btn btn-outline-light">Subscribe</a>
        <ul class="list-inline">
          <li class="list-inline-item facebook-share-icon">
            <a href="https://www.facebook.com/IndiaSpend" target="_blank"><i class="fa fa-facebook" aria-hidden="true"></i></a>
          </li>
          <li class="list-inline-item twitter-share-icon">
            <a href="https://twitter.com/IndiaSpend" target="_blank"><i class="fa fa-twitter" aria-hidden="true"></i></a>
          </li>
          <li class="list-inline-item linkedin-share-icon">
            <a href="https://www.youtube.com/user/IndiaSpend" target="_blank"><i class="fa fa-youtube-play" aria-hidden="true"></i></a>
          </li>
        </ul>
      </section>
    </div>
    <div class="text-right">
      <p class="subtitle">Copyright © All rights reserved</p>
    </div>
  </div>
</footer>
