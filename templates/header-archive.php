<header class="banner">
  <?php get_template_part('partials/nav'); ?>
  <div class="page-header container">
    <div class="row">
      <div class="col-12">
        <?php if (is_category()) : ?>
          <h1 class="category-label h3"><?php single_cat_title(); ?></h1>
        <?php elseif (is_author()) : ?>
          <?php $author_image = get_field('image', 'user_' . get_the_author_meta('ID')) ?>
          <div class="media">
            <?php if ($author_image) : ?>
              <img src="<?= $author_image['sizes']['indiaspend-post-featured-image-small-150w'] ?>" alt="" class="rounded-circle mr-3">
            <?php else : ?>
              <?=
                get_avatar(
                  get_the_author_meta('ID'),
                  125,
                  false,
                  get_the_author_meta('display_name'),
                  array( 'class' => 'rounded-circle mr-3' )
                );
              ?>
            <?php endif; ?>
            <div class="media-body">
              <h1 class="author-page-title"><?php the_author(); ?></h1>
              <p class="subtitle"><?php the_author_meta('description'); ?></p>
            </div>
          </div>
        <?php endif; ?>
      </div>
    </div>
  </div>
</header>
