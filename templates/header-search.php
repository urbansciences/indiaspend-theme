<header class="banner">
  <?php get_template_part('partials/nav'); ?>
  <div class="page-header container">
    <div class="row">
      <div class="col-12">
        <h1 class="category-label h3">Search Query: <?= get_search_query(); ?></h1>
      </div>
    </div>
  </div>
</header>
