<header class="banner">
  <?php get_template_part('partials/nav'); ?>
  <h1 class="text-center py-3 bg-primary text-white"><?php the_title(); ?></h1>
</header>
