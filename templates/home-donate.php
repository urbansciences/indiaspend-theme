<?php global $section; ?>

<div class="py-5 bg-primary">
  <div class="container">
    <div class="row align-items-center">
      <div class="col-12 col-md-8 m-auto text-center text-white">
        <?php if (!empty($section['image'])) : ?>
          <img src="<?= $section['image']['url']; ?>" alt="Donate to IndiaSpend">
        <?php else : ?>
          <img src="<?= get_stylesheet_directory_uri(); ?>/assets/images/donate-indiaspend.svg" alt="Donate to IndiaSpend">
        <?php endif; ?>
        <h2 class="h4"><?= $section['title']; ?></h2>
        <p class="subtitle mb-2"><?= $section['description']; ?></p>
        <a href="<?= $section['button_link']; ?>" class="btn btn-light text-primary">Donate</a>
      </div>
    </div>
  </div>
</div>
