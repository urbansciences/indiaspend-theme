<?php
global $do_not_duplicate, $carousel_number_of_stories, $category_id;
?>

<div id="homepageCarousel" class="carousel slide" data-ride="carousel">
  <div class="carousel-inner">
    <?php
      if ($carousel_number_of_stories !== null) {
        $carousel_number_of_stories = (int) $carousel_number_of_stories;
      } else {
        $carousel_number_of_stories = 5;
      }
      $args = array(
        'posts_per_page' => $carousel_number_of_stories,
        'post_type'    => 'post',
        'category__and' => [$category_id, get_cat_ID('Cover Story')]
      );
    ?>
    <?php $featured_posts_query = new WP_Query($args); ?>
    <?php while ($featured_posts_query->have_posts()) : $featured_posts_query->the_post(); ?>
    <?php $do_not_duplicate[] = $post->ID; ?>
      <?php if ($featured_posts_query->current_post === 0) : ?>
        <div class="carousel-item active">
      <?php else : ?>
        <div class="carousel-item">
      <?php endif; ?>
      <?php if (has_post_thumbnail()) : ?>
        <?php
          $id = get_post_thumbnail_id();
          $alt = get_post_meta($id, '_wp_attachment_image_alt', true);

          $featured_image_desktop_src = wp_get_attachment_image_src($id, 'indiaspend-post-featured-image-desktop');

          $featured_image_mobile_src = wp_get_attachment_image_src($id, 'indiaspend-post-featured-image-mobile');
        ?>
        <a href="<?php the_permalink(); ?>">
          <picture>
            <source media="(min-width: 768px)"
                    srcset="<?php echo esc_attr($featured_image_desktop_src[0]); ?> 1440w">
            <img class="d-block w-100" src="<?php echo esc_attr($featured_image_mobile_src[0]); ?>"
                srcset="<?php echo esc_attr($featured_image_mobile_src[0]); ?> 768w"
                sizes="100vw"
                alt="Card image cap">
          </picture>
        </a>
      <?php endif; ?>
        <div class="carousel-caption">
          <h2 class="carousel-title text-left">
            <a href="<?php the_permalink(); ?>">
              <?php the_title(); ?>
            </a>
          </h2>
        </div>
      </div>
    <?php endwhile; ?>
  </div>
  <a class="carousel-control-prev" href="#homepageCarousel" role="button" data-slide="prev">
    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="carousel-control-next" href="#homepageCarousel" role="button" data-slide="next">
    <span class="carousel-control-next-icon" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a>
</div>
