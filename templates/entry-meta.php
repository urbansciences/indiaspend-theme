<div class="entry-meta-horizontal">
  <p class="byline vcard subtitle">
    <?php coauthors_posts_links(null, ', ', null, null, true); ?>
  </p><time class="updated subtitle" datetime="<?= get_post_time('c', true); ?>"><?= get_the_date(); ?></time>
</div>
