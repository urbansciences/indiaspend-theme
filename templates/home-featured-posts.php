<?php
global $do_not_duplicate, $section;
$featured_posts = $section['stories'];
?>

<?php if ($featured_posts) : ?>

<div id="homepageCarousel" class="carousel slide" data-ride="carousel">
  <div class="carousel-inner">
    <?php foreach ($featured_posts as $index => $post) : ?>
      <?php $do_not_duplicate[] = $post->ID; ?>
      <?php setup_postdata($post); ?>
      <?php if ($index === 0) : ?>
        <div class="carousel-item active">
      <?php else : ?>
        <div class="carousel-item">
      <?php endif; ?>
      <?php if (has_post_thumbnail()) : ?>
        <?php
          $id = get_post_thumbnail_id();
          $alt = get_post_meta($id, '_wp_attachment_image_alt', true);

          $featured_image_desktop_src = wp_get_attachment_image_src($id, 'indiaspend-post-featured-image-desktop');

          $featured_image_mobile_src = wp_get_attachment_image_src($id, 'indiaspend-post-featured-image-mobile');
        ?>
        <a href="<?php the_permalink(); ?>">
          <picture>
            <source media="(min-width: 768px)"
                    srcset="<?php echo esc_attr($featured_image_desktop_src[0]); ?> 1440w">
            <img class="d-block w-100" src="<?php echo esc_attr($featured_image_mobile_src[0]); ?>"
                srcset="<?php echo esc_attr($featured_image_mobile_src[0]); ?> 768w"
                sizes="100vw"
                alt="Card image cap">
          </picture>
        </a>
      <?php endif; ?>
        <div class="carousel-caption">
          <h2 class="carousel-title text-left">
            <a href="<?php the_permalink(); ?>">
              <?php the_title(); ?>
              </a>
          </h2>
        </div>
      </div>
    <?php endforeach; ?>
  </div>

  <?php wp_reset_postdata(); ?>

  <a class="carousel-control-prev" href="#homepageCarousel" role="button" data-slide="prev">
    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="carousel-control-next" href="#homepageCarousel" role="button" data-slide="next">
    <span class="carousel-control-next-icon" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a>
</div>

<?php endif; ?>
