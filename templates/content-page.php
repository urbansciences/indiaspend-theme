<?php if (get_the_content()): ?>
<div class="row justify-content-center">
  <article class="col-10 col-md-8 py-5">
    <?php the_content(); ?>
    <?php wp_link_pages(['before' => '<nav class="page-nav"><p>' . __('Pages:', 'sage'), 'after' => '</p></nav>']); ?>
  </article>
</div>
<?php endif; ?>
