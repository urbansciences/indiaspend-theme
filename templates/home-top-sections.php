<?php
global $do_not_duplicate, $section, $top_section_categories;
?>

<div class="container top-sections">
  <header class="border-bottom">
    <h2 class="h6"><?= $section['section_title']; ?></h2>
    <h3 class="subtitle text-muted"><?= $section['section_subtitle']; ?></h3>
  </header>
  <section>
    <?php
      $category_id = $top_section_categories[0]->term_id;
      $category_link = get_category_link($category_id);
      $category_name = $top_section_categories[0]->name;
    ?>
    <a href="<?= $category_link; ?>"><h3 class="h6 category-label"><?= $category_name ?></h3></a>
    <div class="row">
      <?php
        $args = array(
          'category_name'   => $category_name,
          'post_type'       => 'post',
          'posts_per_page'  => 2,
          'post__not_in'    => $do_not_duplicate
        );
      ?>
      <?php $top_section_category_one_query = new WP_Query($args); ?>
      <?php while ($top_section_category_one_query->have_posts()) : $top_section_category_one_query->the_post(); ?>
      <?php $do_not_duplicate[] = $post->ID; ?>
        <article class="post-large">
          <header>
            <?php if (has_post_thumbnail()) : ?>
              <?php
                $id = get_post_thumbnail_id();
                $alt = get_post_meta($id, '_wp_attachment_image_alt', true);

                $featured_image_large_src = wp_get_attachment_image_src($id, 'indiaspend-post-featured-image-large');
                $featured_image_large_srcset = wp_get_attachment_image_srcset($id, 'indiaspend-post-featured-image-large');
              ?>
              <a href="<?php the_permalink(); ?>">
                <img src="<?php echo esc_attr($featured_image_large_src[0]); ?>"
                    srcset="<?php echo esc_attr($featured_image_large_srcset); ?>"
                    sizes="(min-width: 1200px) 540px,
                           (min-width: 992px) 450px,
                           (min-width: 768px) 330px,
                           (min-width: 576px) 510px,
                           545px"
                    alt="<?php echo esc_attr($alt);?>"
                    class="img-fluid">
              </a>
            <?php endif; ?>
            <h3 class="entry-title">
              <a href="<?php the_permalink(); ?>">
                <?php the_title(); ?>
              </a>
            </h3>
            <div class="entry-info">
              <address class="vcard subtitle">
                <?php coauthors_posts_links(null, ', ', null, null, true); ?>
              </address>
              <time class="text-muted subtitle updated d-block mb-3" datetime="<?= get_post_time('c', true); ?>"><?= get_the_date(); ?></time>
            </div>
          </header>
        </article>
      <?php endwhile; ?>
    </div>
    <div class="row">
      <section class="section-small">
        <?php
          $category_id = $top_section_categories[1]->term_id;
          $category_link = get_category_link($category_id);
          $category_name = $top_section_categories[1]->name;
        ?>
        <a href="<?= $category_link; ?>"><h3 class="h6 category-label"><?= $category_name; ?></h3></a>
        <div class="row">
          <?php
            $args = array(
              'category_name'   => $category_name,
              'post_type'       => 'post',
              'posts_per_page'  => 2,
              'post__not_in'    => $do_not_duplicate
            );
          ?>
          <?php $top_section_category_two_query = new WP_Query($args); ?>
          <?php while ($top_section_category_two_query->have_posts()) : $top_section_category_two_query->the_post(); ?>
          <?php $do_not_duplicate[] = $post->ID; ?>
            <article class="post-small">
              <header class="row">
                <div class="post-image-container">
                  <?php if (has_post_thumbnail()) : ?>
                    <?php
                      $id = get_post_thumbnail_id();
                      $alt = get_post_meta($id, '_wp_attachment_image_alt', true);

                      $featured_image_small_src = wp_get_attachment_image_src($id, 'indiaspend-post-featured-image-small');
                      $featured_image_small_srcset = wp_get_attachment_image_srcset($id, 'indiaspend-post-featured-image-small');
                    ?>
                    <a href="<?php the_permalink(); ?>">
                      <img class="post-image img-fluid" src="<?php echo esc_attr($featured_image_small_src[0]); ?>"
                        srcset="<?php echo esc_attr($featured_image_small_srcset); ?>"
                        sizes="(min-width: 1200px) 255px,
                               (min-width: 992px) 210px,
                               (min-width: 768px) 150px,
                               (min-width: 576px) 195px,
                               209.578px"
                        alt="<?php echo esc_attr($alt);?>">
                    </a>
                  <?php endif; ?>
                </div>
                <div class="entry-meta">
                  <h3 class="h4 entry-title">
                    <a href="<?php the_permalink(); ?>">
                      <?php the_title(); ?>
                    </a>
                  </h3>
                  <div class="entry-info">
                    <address class="vcard subtitle">
                      <?php the_author_posts_link(); ?>
                    </address>
                    <time class="text-muted subtitle updated" datetime="<?= get_post_time('c', true); ?>"><?= get_the_date(); ?></time>
                  </div>
                </div>
              </header>
            </article>
          <?php endwhile; ?>
        </div>
      </section>
      <section class="section-small">
        <?php
          $category_id = $top_section_categories[2]->term_id;
          $category_link = get_category_link($category_id);
          $category_name = $top_section_categories[2]->name;
        ?>
        <a href="<?= $category_link; ?>"><h3 class="h6 category-label"><?= $category_name; ?></h3></a>
        <div class="row">
          <?php
            $args = array(
              'category_name'   => $category_name,
              'post_type'       => 'post',
              'posts_per_page'  => 2,
              'post__not_in'    => $do_not_duplicate
            );
          ?>
          <?php $top_section_category_three_query = new WP_Query($args); ?>
          <?php while ($top_section_category_three_query->have_posts()) : $top_section_category_three_query->the_post(); ?>
          <?php $do_not_duplicate[] = $post->ID; ?>
            <article class="post-small">
              <header class="row">
                <div class="post-image-container">
                  <?php if (has_post_thumbnail()) : ?>
                    <?php
                      $id = get_post_thumbnail_id();
                      $alt = get_post_meta($id, '_wp_attachment_image_alt', true);

                      $featured_image_small_src = wp_get_attachment_image_src($id, 'indiaspend-post-featured-image-small');
                      $featured_image_small_srcset = wp_get_attachment_image_srcset($id, 'indiaspend-post-featured-image-small');
                    ?>
                    <a href="<?php the_permalink(); ?>">
                      <img class="post-image img-fluid" src="<?php echo esc_attr($featured_image_small_src[0]); ?>"
                        srcset="<?php echo esc_attr($featured_image_small_srcset); ?>"
                        sizes="(min-width: 1200px) 255px,
                               (min-width: 992px) 210px,
                               (min-width: 768px) 150px,
                               (min-width: 576px) 195px,
                               209.578px"
                        alt="<?php echo esc_attr($alt);?>">
                    </a>
                  <?php endif; ?>
                </div>
                <div class="entry-meta">
                  <h3 class="h4 entry-title">
                    <a href="<?php the_permalink(); ?>">
                      <?php the_title(); ?>
                    </a>
                  </h3>
                  <div class="entry-info">
                    <address class="vcard subtitle">
                      <?php the_author_posts_link(); ?>
                    </address>
                    <time class="text-muted subtitle updated" datetime="<?= get_post_time('c', true); ?>"><?= get_the_date(); ?></time>
                  </div>
                </div>
              </header>
            </article>
          <?php endwhile; ?>
        </div>
      </section>
    </div>
    <div class="row">
      <section class="section-small">
        <?php
          $category_id = $top_section_categories[3]->term_id;
          $category_link = get_category_link($category_id);
          $category_name = $top_section_categories[3]->name;
        ?>
        <a href="<?= $category_link; ?>"><h3 class="h6 category-label"><?= $category_name; ?></h3></a>
        <div class="row">
          <?php
            $args = array(
              'category_name'   => $category_name,
              'post_type'       => 'post',
              'posts_per_page'  => 2,
              'post__not_in'    => $do_not_duplicate
            );
          ?>
          <?php $top_section_category_four_query = new WP_Query($args); ?>
          <?php while ($top_section_category_four_query->have_posts()) : $top_section_category_four_query->the_post(); ?>
          <?php $do_not_duplicate[] = $post->ID; ?>
            <article class="post-small">
              <header class="row">
                <div class="post-image-container">
                  <?php if (has_post_thumbnail()) : ?>
                    <?php
                      $id = get_post_thumbnail_id();
                      $alt = get_post_meta($id, '_wp_attachment_image_alt', true);

                      $featured_image_small_src = wp_get_attachment_image_src($id, 'indiaspend-post-featured-image-small');
                      $featured_image_small_srcset = wp_get_attachment_image_srcset($id, 'indiaspend-post-featured-image-small');
                    ?>
                    <a href="<?php the_permalink(); ?>">
                      <img class="post-image img-fluid" src="<?php echo esc_attr($featured_image_small_src[0]); ?>"
                        srcset="<?php echo esc_attr($featured_image_small_srcset); ?>"
                        sizes="(min-width: 1200px) 255px,
                               (min-width: 992px) 210px,
                               (min-width: 768px) 150px,
                               (min-width: 576px) 195px,
                               209.578px"
                        alt="<?php echo esc_attr($alt);?>">
                    </a>
                  <?php endif; ?>
                </div>
                <div class="entry-meta">
                  <h3 class="h4 entry-title">
                    <a href="<?php the_permalink(); ?>">
                      <?php the_title(); ?>
                    </a>
                  </h3>
                  <div class="entry-info">
                    <address class="vcard subtitle">
                      <?php the_author_posts_link(); ?>
                    </address>
                    <time class="text-muted subtitle updated" datetime="<?= get_post_time('c', true); ?>"><?= get_the_date(); ?></time>
                  </div>
                </div>
              </header>
            </article>
          <?php endwhile; ?>
        </div>
      </section>
      <section class="section-small">
        <?php
          $category_id = $top_section_categories[4]->term_id;
          $category_link = get_category_link($category_id);
          $category_name = $top_section_categories[4]->name;
        ?>
        <a href="<?= $category_link; ?>"><h3 class="h6 category-label"><?= $category_name; ?></h3></a>
        <div class="row">
          <?php
            $args = array(
              'category_name'   => $category_name,
              'post_type'       => 'post',
              'posts_per_page'  => 2,
              'post__not_in'    => $do_not_duplicate
            );
          ?>
          <?php $top_section_category_five_query = new WP_Query($args); ?>
          <?php while ($top_section_category_five_query->have_posts()) : $top_section_category_five_query->the_post(); ?>
          <?php $do_not_duplicate[] = $post->ID; ?>
            <article class="post-small">
              <header class="row">
                <div class="post-image-container">
                  <?php if (has_post_thumbnail()) : ?>
                    <?php
                      $id = get_post_thumbnail_id();
                      $alt = get_post_meta($id, '_wp_attachment_image_alt', true);

                      $featured_image_small_src = wp_get_attachment_image_src($id, 'indiaspend-post-featured-image-small');
                      $featured_image_small_srcset = wp_get_attachment_image_srcset($id, 'indiaspend-post-featured-image-small');
                    ?>
                    <a href="<?php the_permalink(); ?>">
                      <img class="post-image img-fluid" src="<?php echo esc_attr($featured_image_small_src[0]); ?>"
                        srcset="<?php echo esc_attr($featured_image_small_srcset); ?>"
                        sizes="(min-width: 1200px) 255px,
                               (min-width: 992px) 210px,
                               (min-width: 768px) 150px,
                               (min-width: 576px) 195px,
                               209.578px"
                        alt="<?php echo esc_attr($alt);?>">
                    </a>
                  <?php endif; ?>
                </div>
                <div class="entry-meta">
                  <h3 class="h4 entry-title">
                    <a href="<?php the_permalink(); ?>">
                      <?php the_title(); ?>
                    </a>
                  </h3>
                  <div class="entry-info">
                    <address class="vcard subtitle">
                      <?php the_author_posts_link(); ?>
                    </address>
                    <time class="text-muted subtitle updated" datetime="<?= get_post_time('c', true); ?>"><?= get_the_date(); ?></time>
                  </div>
                </div>
              </header>
            </article>
          <?php endwhile; ?>
        </div>
      </section>
    </div>
  </section>
</div>
