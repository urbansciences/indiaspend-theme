<?php global $section; ?>

<div class="py-5">
  <div class="container">
    <header class="border-bottom row no-gutters align-items-top mb-3">
      <div class="col-12 col-sm-6">
        <h2 class="h6"><?= $section['section_title']; ?></h2>
        <h3 class="subtitle text-muted"><?= $section['section_subtitle']; ?></h3>
      </div>
      <?php if ($section['link']) : ?>
      <div class="col-12 col-sm-6 text-sm-right pb-3 pb-sm-0">
        <a href="<?= $section['link']; ?>" class="btn btn-outline-primary btn-sm">Visit Full Site</a>
      </div>
      <?php endif; ?>
    </header>
    <div class="row">
      <div class="col-12 col-md-8 my-4 my-md-0">
        <?= $section['embed_code']; ?>
      </div>
      <div class="col-12 col-md-4">
        <a href="<?= get_category_link($section['category']); ?>" class="h6 category-label"><?= get_cat_name($section['category']); ?></a>
        <div class="row">
          <?php
            $args = array(
              'posts_per_page' => 6,
              'post_type'    => 'post',
              'category__in' => $section['category']
            );
          ?>
          <?php $posts_by_category = new WP_Query($args); ?>
          <?php while ($posts_by_category->have_posts()) : $posts_by_category->the_post(); ?>
            <div class="col-12">
              <h3 class="h5">
                <a href="<?php the_permalink(); ?>">
                  <?php the_title(); ?>
                </a>
              </h3>
              <p class="text-muted subtitle"><time class="updated subtitle" datetime="<?= get_post_time('c', true); ?>"><?= get_the_date(); ?></time></p>
            </div>
          <?php endwhile; ?>
        </div>
      </div>
    </div>
  </div>
</div>
