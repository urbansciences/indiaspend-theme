<?php global $do_not_duplicate, $section; ?>

<div class="container">
  <header class="border-bottom">
    <h2 class="h6"><?= $section['section_title']; ?></h2>
    <h3 class="subtitle text-muted"><?= $section['section_subtitle']; ?></h3>
  </header>

  <?php
    $args = array(
      'post_type'       => 'post',
      'posts_per_page'  => (int) $section['max_number_of_stories'],
      'post__not_in'    => $do_not_duplicate
    );
  ?>
  <?php $latest_posts_query = new WP_Query($args); ?>
  <?php while ($latest_posts_query->have_posts()) : $latest_posts_query->the_post(); ?>
    <?php $do_not_duplicate[] = $post->ID; ?>
    <article class="row mb-5">
      <div class="col-4 col-lg-7">
        <?php if (has_post_thumbnail()) : ?>
          <?php
            $id = get_post_thumbnail_id();
            $alt = get_post_meta($id, '_wp_attachment_image_alt', true);

            $featured_image_latest_src = wp_get_attachment_image_src($id, 'indiaspend-post-featured-image-latest');
            $featured_image_latest_srcset = wp_get_attachment_image_srcset($id, 'indiaspend-post-featured-image-latest');
            $featured_image_latest_small_src = wp_get_attachment_image_src($id, 'indiaspend-post-featured-image-small');
            $featured_image_latest_small_srcset = wp_get_attachment_image_srcset($id, 'indiaspend-post-featured-image-small');
          ?>
          <a href="<?php the_permalink(); ?>">
            <picture>
              <source media="(min-width: 992px)"
                      srcset="<?php echo esc_attr($featured_image_latest_srcset); ?>">
              <img class="img-fluid"
                   src="<?php echo esc_attr($featured_image_latest_small_src[0]); ?>"
                   srcset="<?php echo esc_attr($featured_image_latest_small_srcset); ?>"
                   sizes="(min-width: 768px) 240px,
                          (min-width: 576px) 180px,
                          191px"
                   alt="<?php echo esc_attr($alt);?>">
            </picture>
          </a>
        <?php endif; ?>
      </div>
      <header class="col-8 col-lg-5">
        <h3 class="entry-title" class="h4">
          <a href="<?php the_permalink(); ?>">
            <?php the_title(); ?>
          </a>
        </h3>
        <?php get_template_part('templates/entry-meta'); ?>
        <p class="d-none d-sm-block">
          <?= get_the_excerpt(); ?>
        </p>
      </header>
    </article>
  <?php endwhile; ?>

  <?php
    // Ajax Load More
    $post__not_in = '';
    if ($do_not_duplicate) {
      $post__not_in = implode(',', $do_not_duplicate);
    }
    echo do_shortcode('[ajax_load_more pause="true" post__not_in="'.$post__not_in.'"]');
  ?>
</div>
