<?php

use Roots\Sage\Setup;
use Roots\Sage\Wrapper;

?>

<?php while (have_posts()) : the_post(); ?>
  <article class="row mb-5">
    <div class="col-3">
      <?php if (has_post_thumbnail()) : ?>
        <?php
          $id = get_post_thumbnail_id();
          $alt = get_post_meta($id, '_wp_attachment_image_alt', true);

          $featured_image_latest_src = wp_get_attachment_image_src($id, 'indiaspend-post-featured-image-latest');
          $featured_image_latest_srcset = wp_get_attachment_image_srcset($id, 'indiaspend-post-featured-image-latest');
          $featured_image_latest_small_src = wp_get_attachment_image_src($id, 'indiaspend-post-featured-image-small');
          $featured_image_latest_small_srcset = wp_get_attachment_image_srcset($id, 'indiaspend-post-featured-image-small');
        ?>
        <a href="<?php the_permalink(); ?>">
          <img class="img-fluid"
               src="<?php echo esc_attr($featured_image_latest_small_src[0]); ?>"
               srcset="<?php echo esc_attr($featured_image_latest_small_srcset); ?>"
               sizes="(min-width: 768px) 240px,
                      (min-width: 576px) 180px,
                      191px"
               alt="<?php echo esc_attr($alt);?>">
        </a>
      <?php endif; ?>
    </div>
    <header class="col-9">
      <h3 class="entry-title" class="h4">
        <a href="<?php the_permalink(); ?>">
          <?php the_title(); ?>
        </a>
      </h3>
      <?php get_template_part('templates/entry-meta'); ?>
      <p class="d-none d-sm-block">
        <?= get_the_excerpt(); ?>
      </p>
    </header>
  </article>
<?php endwhile; ?>
