<?php
/**
 * Template Name: Contact Template
 */
?>

<?php while (have_posts()) : the_post(); ?>
  <div class="container py-4 text-center">
    <div class="row">
      <div class="col-12 col-md-8 offset-md-2">
        <?php get_template_part('templates/content', 'page'); ?>
        <section class="py-2">
          <h2 class="h4">Get in Touch</h2>
          <p><b>General:</b> <a href="mailto:research@indiaspend.org">research@indiaspend.org</a><br>
          <b>Careers:</b> <a href="mailto:jobs@indiaspend.org">jobs@indiaspend.org</a><br>
          <b>Letters to the Editor:</b> <a href="mailto:editor@indiaspend.org">editor@indiaspend.org</a></p>
        </section>
        <p><b>Phone Number:</b> <a href="tel:+912266505867">+912266505867</a></p>
        <section class="py-2">
          <h2 class="h4">Our Address</h2>
          <p><b>Spending & Policy Research Foundation</b></p>
          <p>319, Adhyaru Industrial Estate,<br>
          Sun Mill Compound,<br>
          Lower Parel, Mumbai – 400013</p>
        </section>
      </div>
    </div>
  </div>
  <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3772.5468821877134!2d72.82508001494116!3d18.995608587134047!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3be7ce8cc8093c59%3A0x3916445672d074f!2sSun+Mill+Compound!5e0!3m2!1sen!2sin!4v1510070673187" width="100%" height="600" frameborder="0" style="border:0" allowfullscreen></iframe>
<?php endwhile; ?>
