<?php

namespace Roots\Sage\Setup;

use Roots\Sage\Assets;


add_action('after_setup_theme', __NAMESPACE__ . '\\setup');

/**
 * Theme setup
 */
function setup() {
  // Enable features from Soil when plugin is activated
  // https://roots.io/plugins/soil/
  add_theme_support('soil-clean-up');
  add_theme_support('soil-nice-search');
  add_theme_support('soil-jquery-cdn');
  add_theme_support('soil-relative-urls');

  // Make theme available for translation
  // Community translations can be found at https://github.com/roots/sage-translations
  load_theme_textdomain('sage', get_template_directory() . '/lang');

  // Enable plugins to manage the document title
  // http://codex.wordpress.org/Function_Reference/add_theme_support#Title_Tag
  add_theme_support('title-tag');

  // Register wp_nav_menu() menus
  // http://codex.wordpress.org/Function_Reference/register_nav_menus
  register_nav_menus([
    'primary_navigation' => __('Primary Navigation', 'sage'),
    'secondary_navigation' => __('Secondary Navigation', 'sage'),
  ]);

  // Enable post thumbnails
  // http://codex.wordpress.org/Post_Thumbnails
  // http://codex.wordpress.org/Function_Reference/set_post_thumbnail_size
  // http://codex.wordpress.org/Function_Reference/add_image_size
  add_theme_support('post-thumbnails');
  add_image_size('indiaspend-post-featured-image-desktop', 1440, 563, true);
  add_image_size('indiaspend-post-featured-image-mobile', 768, 530, true);

  add_image_size('indiaspend-post-featured-image-large', 540, 296.7032967033, true);
  add_image_size('indiaspend-post-featured-image-large-1080w', 1080, 593.4065934066, true);
  add_image_size('indiaspend-post-featured-image-large-900w', 900, 494.5054945055, true);
  add_image_size('indiaspend-post-featured-image-large-600w', 660, 362.6373626374, true);
  add_image_size('indiaspend-post-featured-image-large-450w', 450, 247.2527472527, true);
  add_image_size('indiaspend-post-featured-image-large-330w', 330, 181.3186813187, true);
  add_image_size('indiaspend-post-featured-image-large-240w', 240, 131.8681318681, true);

  add_image_size('indiaspend-post-featured-image-small', 255, 255, true);
  add_image_size('indiaspend-post-featured-image-small-105w', 105, 105, true);
  add_image_size('indiaspend-post-featured-image-small-150w', 150, 150, true);
  add_image_size('indiaspend-post-featured-image-small-420w', 420, 420, true);
  add_image_size('indiaspend-post-featured-image-small-510w', 510, 510, true);


  add_image_size('indiaspend-post-featured-image-latest', 1060, 414.4305555556, true);
  add_image_size('indiaspend-post-featured-image-latest-530w', 530, 207.2152777778, true);


  // Enable HTML5 markup support
  // http://codex.wordpress.org/Function_Reference/add_theme_support#HTML5
  add_theme_support('html5', ['caption', 'comment-form', 'comment-list', 'gallery', 'search-form']);

  // Use main stylesheet for visual editor
  // To add custom styles edit /assets/styles/layouts/_tinymce.scss
  add_editor_style(Assets\asset_path('styles/main.css'));
}


add_action('widgets_init', __NAMESPACE__ . '\\widgets_init');

/**
 * Register sidebars
 */
function widgets_init() {
  register_sidebar([
    'name'          => __('Primary', 'sage'),
    'id'            => 'sidebar-primary',
    'before_widget' => '<section class="widget %1$s %2$s">',
    'after_widget'  => '</section>',
    'before_title'  => '<h3>',
    'after_title'   => '</h3>'
  ]);

  register_sidebar([
    'name'          => __('Footer', 'sage'),
    'id'            => 'sidebar-footer',
    'before_widget' => '<section class="widget %1$s %2$s">',
    'after_widget'  => '</section>',
    'before_title'  => '<h3>',
    'after_title'   => '</h3>'
  ]);
}


function get_main_categories() {
  return ['earth-check', 'health-check', 'education-check', 'gender-check'];
}


/**
 * Determine which pages should NOT display the sidebar
 */
function display_sidebar() {
  static $display;

  isset($display) || $display = !in_array(true, [
    // The sidebar will NOT be displayed if ANY of the following return true.
    // @link https://codex.wordpress.org/Conditional_Tags
    is_404(),
    is_front_page(),
    is_page_template('template-contact.php'),
    is_page_template('template-subscribe.php'),
    is_page_template('template-donate.php'),
    is_page_template('template-team.php'),
    is_page_template('searchpage.php'),
    is_singular('page'),
    is_category(get_main_categories()),
  ]);

  return apply_filters('sage/display_sidebar', $display);
}


add_action('wp_enqueue_scripts', __NAMESPACE__ . '\\assets', 100);

/**
 * Theme assets
 */
function assets() {
  wp_enqueue_style('sage/css', Assets\asset_path('styles/main.css'), false, null);

  if (is_single() && comments_open() && get_option('thread_comments')) {
    wp_enqueue_script('comment-reply');
  }

  wp_enqueue_script('bootstrap/popper', 'https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.6/umd/popper.min.js', ['jquery'], null, true);
  wp_enqueue_script('bootstrap/js', 'https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.0.0-beta/js/bootstrap.min.js', ['bootstrap/popper'], null, true);
  wp_enqueue_script('sage/js', Assets\asset_path('scripts/main.js'), ['bootstrap/js'], null, true);
}


add_action('admin_head', __NAMESPACE__ . '\\indiaspend_remove_editor_from_posts');

/**
 * Remove support for editor on Post pages
 */
function indiaspend_remove_editor_from_posts() {
  remove_post_type_support('post', 'editor');
  if((int) get_option('page_on_front') === get_the_ID()) {
    remove_post_type_support('page', 'editor');
  }
}


/**
 * Include the TGM_Plugin_Activation class.
 *
 * Depending on your implementation, you may want to change the include call:
 *
 * Parent Theme:
 * require_once get_template_directory() . '/path/to/class-tgm-plugin-activation.php';
 *
 * Child Theme:
 * require_once get_stylesheet_directory() . '/path/to/class-tgm-plugin-activation.php';
 *
 * Plugin:
 * require_once dirname( __FILE__ ) . '/path/to/class-tgm-plugin-activation.php';
 */
require_once get_template_directory() . '/inc/class-tgm-plugin-activation.php';


add_action('tgmpa_register', __NAMESPACE__ . '\\indiaspend_register_required_plugins');

/**
 * Register the required plugins for this theme.
 *
 * In this example, we register five plugins:
 * - one included with the TGMPA library
 * - two from an external source, one from an arbitrary source, one from a GitHub repository
 * - two from the .org repo, where one demonstrates the use of the `is_callable` argument
 *
 * The variables passed to the `tgmpa()` function should be:
 * - an array of plugin arrays;
 * - optionally a configuration array.
 * If you are not changing anything in the configuration array, you can remove the array and remove the
 * variable from the function call: `tgmpa( $plugins );`.
 * In that case, the TGMPA default settings will be used.
 *
 * This function is hooked into `tgmpa_register`, which is fired on the WP `init` action on priority 10.
 */
function indiaspend_register_required_plugins() {
  /*
   * Array of plugin arrays. Required keys are name and slug.
   * If the source is NOT from the .org repo, then source is also required.
   */
  $plugins = array(

    array(
    'name'      => 'Co-Authors Plus',
    'slug'      => 'co-authors-plus',
    'required'  => true,
    'force_activation'   => true,
    ),

    array(
    'name'      => 'Minimum Featured Image Size',
    'slug'      => 'minimum-featured-image-size',
    'required'  => true,
    'force_activation'   => true,
    ),

    array(
    'name'      => 'My Eyes Are Up Here',
    'slug'      => 'my-eyes-are-up-here',
    'required'  => true,
    'force_activation'   => true,
    ),

    array(
    'name'      => 'Related Posts by Taxonomy',
    'slug'      => 'related-posts-by-taxonomy',
    'required'  => true,
    'force_activation'   => true,
    ),

    array(
    'name'      => 'Trending/Popular Post Slider and Widget',
    'slug'      => 'wp-trending-post-slider-and-widget',
    'required'  => true,
    'force_activation'   => true,
    ),

    array(
    'name'      => 'WordPress Infinite Scroll – Ajax Load More',
    'slug'      => 'ajax-load-more',
    'required'  => true,
    'force_activation'   => true,
    ),

  );

  /*
   * Array of configuration settings. Amend each line as needed.
   *
   * TGMPA will start providing localized text strings soon. If you already have translations of our standard
   * strings available, please help us make TGMPA even better by giving us access to these translations or by
   * sending in a pull-request with .po file(s) with the translations.
   *
   * Only uncomment the strings in the config array if you want to customize the strings.
   */
  $config = array(
  'id'           => 'indiaspend',                 // Unique ID for hashing notices for multiple instances of TGMPA.
  'default_path' => '',                      // Default absolute path to bundled plugins.
  'menu'         => 'tgmpa-install-plugins', // Menu slug.
  'parent_slug'  => 'themes.php',            // Parent menu slug.
  'capability'   => 'edit_theme_options',    // Capability needed to view plugin install page, should be a capability associated with the parent menu used.
  'has_notices'  => true,                    // Show admin notices or not.
  'dismissable'  => false,                    // If false, a user cannot dismiss the nag message.
  'dismiss_msg'  => '',                      // If 'dismissable' is false, this message will be output at top of nag.
  'is_automatic' => true,                   // Automatically activate plugins after installation or not.
  'message'      => '',                      // Message to output right before the plugins table.

  /*
  'strings'      => array(
  'page_title'                      => __( 'Install Required Plugins', 'indiaspend' ),
  'menu_title'                      => __( 'Install Plugins', 'indiaspend' ),
  /* translators: %s: plugin name. * /
  'installing'                      => __( 'Installing Plugin: %s', 'indiaspend' ),
  /* translators: %s: plugin name. * /
  'updating'                        => __( 'Updating Plugin: %s', 'indiaspend' ),
  'oops'                            => __( 'Something went wrong with the plugin API.', 'indiaspend' ),
  'notice_can_install_required'     => _n_noop(
  /* translators: 1: plugin name(s). * /
  'This theme requires the following plugin: %1$s.',
  'This theme requires the following plugins: %1$s.',
  'indiaspend'
  ),
  'notice_can_install_recommended'  => _n_noop(
  /* translators: 1: plugin name(s). * /
  'This theme recommends the following plugin: %1$s.',
  'This theme recommends the following plugins: %1$s.',
  'indiaspend'
  ),
  'notice_ask_to_update'            => _n_noop(
  /* translators: 1: plugin name(s). * /
  'The following plugin needs to be updated to its latest version to ensure maximum compatibility with this theme: %1$s.',
  'The following plugins need to be updated to their latest version to ensure maximum compatibility with this theme: %1$s.',
  'indiaspend'
  ),
  'notice_ask_to_update_maybe'      => _n_noop(
  /* translators: 1: plugin name(s). * /
  'There is an update available for: %1$s.',
  'There are updates available for the following plugins: %1$s.',
  'indiaspend'
  ),
  'notice_can_activate_required'    => _n_noop(
  /* translators: 1: plugin name(s). * /
  'The following required plugin is currently inactive: %1$s.',
  'The following required plugins are currently inactive: %1$s.',
  'indiaspend'
  ),
  'notice_can_activate_recommended' => _n_noop(
  /* translators: 1: plugin name(s). * /
  'The following recommended plugin is currently inactive: %1$s.',
  'The following recommended plugins are currently inactive: %1$s.',
  'indiaspend'
  ),
  'install_link'                    => _n_noop(
  'Begin installing plugin',
  'Begin installing plugins',
  'indiaspend'
  ),
  'update_link' 					  => _n_noop(
  'Begin updating plugin',
  'Begin updating plugins',
  'indiaspend'
  ),
  'activate_link'                   => _n_noop(
  'Begin activating plugin',
  'Begin activating plugins',
  'indiaspend'
  ),
  'return'                          => __( 'Return to Required Plugins Installer', 'indiaspend' ),
  'plugin_activated'                => __( 'Plugin activated successfully.', 'indiaspend' ),
  'activated_successfully'          => __( 'The following plugin was activated successfully:', 'indiaspend' ),
  /* translators: 1: plugin name. * /
  'plugin_already_active'           => __( 'No action taken. Plugin %1$s was already active.', 'indiaspend' ),
  /* translators: 1: plugin name. * /
  'plugin_needs_higher_version'     => __( 'Plugin not activated. A higher version of %s is needed for this theme. Please update the plugin.', 'indiaspend' ),
  /* translators: 1: dashboard link. * /
  'complete'                        => __( 'All plugins installed and activated successfully. %1$s', 'indiaspend' ),
  'dismiss'                         => __( 'Dismiss this notice', 'indiaspend' ),
  'notice_cannot_install_activate'  => __( 'There are one or more required or recommended plugins to install, update or activate.', 'indiaspend' ),
  'contact_admin'                   => __( 'Please contact the administrator of this site for help.', 'indiaspend' ),

  'nag_type'                        => '', // Determines admin notice type - can only be one of the typical WP notice classes, such as 'updated', 'update-nag', 'notice-warning', 'notice-info' or 'error'. Some of which may not work as expected in older WP versions.
  ),
  */
  );

  tgmpa($plugins, $config);
}

add_filter( 'admin_post_thumbnail_html', __NAMESPACE__ . '\\add_featured_image_instruction');

function add_featured_image_instruction( $content ) {
    return $content .= '<p class="howto">Minimum Width: 1440px<br>Minimum Height: 563px</p>';
}


add_action('load-themes.php', __NAMESPACE__ . '\\indiaspend_add_theme_caps');

function indiaspend_add_theme_caps() {
  global $pagenow;

  // gets the author role
  $role = get_role('editor');

  if ('themes.php' == $pagenow && isset($_GET['activated'])) { // Test if theme is activated
    // Theme is activated
    // Allow editor to add users
    $role->add_cap('edit_users');
    $role->add_cap('list_users');
    $role->add_cap('promote_users');
    $role->add_cap('create_users');
    $role->add_cap('add_users');
    $role->add_cap('delete_users');
  } else {
    // Theme is deactivated
    // Remove the capability when theme is deactivated
    $role->remove_cap('edit_users');
    $role->remove_cap('list_users');
    $role->remove_cap('promote_users');
    $role->remove_cap('create_users');
    $role->remove_cap('add_users');
    $role->remove_cap('delete_users');
  }
}


add_action('new_post', __NAMESPACE__ . '\\indiaspend_add_featured_post', 10, 2);

function indiaspend_add_featured_post($post_id, $post) {
  if (has_category('cover-story', $post)) {
    $frontpage_id = (int) get_option('page_on_front');
    $home_page_sections = get_field('sections', $frontpage_id);
    for ($i=0; $i < count($home_page_sections); $i++) {
      if ($home_page_sections[$i]['acf_fc_layout'] === 'carousel') {
        array_unshift($home_page_sections[$i]['stories'], $post);
        $home_page_sections[$i]['stories'] = array_slice($home_page_sections[$i]['stories'], 0, 5);
        break;
      }
    }
    update_field('sections', $home_page_sections, $frontpage_id);
  }
}
