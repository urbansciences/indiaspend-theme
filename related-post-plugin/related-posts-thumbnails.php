<?php
/**
 * Widget and shortcode template: post thumbnails template
 *
 * This template is used by the plugin: Related Posts by Taxonomy.
 *
 * plugin:        https://wordpress.org/plugins/related-posts-by-taxonomy
 * Documentation: https://keesiemeijer.wordpress.com/related-posts-by-taxonomy/
 *
 * @package Related Posts by Taxonomy
 * @since 0.3
 *
 * The following variables are available:
 *
 * @var array  $related_posts Array with full related posts objects or empty array.
 * @var array  $rpbt_args     Array with widget or shortcode arguments.
 *
 * deprecated (since version 0.3)
 * @var string $image_size    Image size. (deprecated - use $rpbt_args['image_size'] instead)
 * @var string $columns       Columns.    (deprecated - use $rpbt_args['columns'] instead)
 */
?>

<?php
/**
 * Note: global $post; is used before this template by the widget and the shortcode.
 */
?>

<?php if ($related_posts) : ?>

  <?php foreach ($related_posts as $post) : ?>
    <article class="related-post">
      <?php setup_postdata($post); ?>
      <?php $size = isset( $rpbt_args['size'] ) ? $size : 'indiaspend-post-featured-image-large-330w'; ?>
      <?php if (has_post_thumbnail()) : ?>
        <a href="<?php the_permalink(); ?>"><?php the_post_thumbnail($size, ['class' => 'img-fluid']); ?></a>
      <?php endif; ?>
      <a href="<?php the_permalink(); ?>"><h6 class="entry-title"><?php the_title(); ?></h6></a>
      <time class="updated subtitle" datetime="<?= get_post_time('c', true); ?>"><?= get_the_date(); ?></time>
    </article>
  <?php endforeach; ?>

<?php else : ?>
  <p><?php _e('No related posts found', 'related-posts-by-taxonomy'); ?></p>
<?php endif; ?>

<?php
/**
 * note: wp_reset_postdata(); is used after this template by the widget and the shortcode
 */
?>
