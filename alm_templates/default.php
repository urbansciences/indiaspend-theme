<?php

use Roots\Sage\Setup;
use Roots\Sage\Wrapper;

?>

<?php if (is_single()): ?>
  <article <?php post_class(); ?>>
    <header>
      <div class="container entry-header">
        <h1 class="entry-title h2"><?php the_title(); ?></h1>
        <?php get_template_part('templates/entry-meta'); ?>
      </div>
      <?php get_template_part('partials/featured-image'); ?>
    </header>
    <div class="container">
      <div class="row">
        <?php get_template_part('partials/share-buttons'); ?>
        <div class="entry-wrap">
          <div class="entry-content">
            <?php if (have_rows('content')) : ?>
              <?php while (have_rows('content')) : the_row(); ?>
                <?php if (get_row_layout() === 'text') : ?>
                  <?php the_sub_field('text'); ?>
                <?php elseif (get_row_layout() === 'dataviz_embed') : ?>
                  <?php if (empty(get_sub_field('tablet_embed_code')) && empty(get_sub_field('mobile_embed_code'))) : ?>
                    <div class="dataviz dataviz-desktop-tablet-mobile">
                      <?php the_sub_field('desktop_embed_code'); ?>
                    </div>
                  <?php elseif (!empty(get_sub_field('tablet_embed_code')) && empty(get_sub_field('mobile_embed_code'))) : ?>
                    <div class="dataviz dataviz-desktop">
                      <?php the_sub_field('desktop_embed_code'); ?>
                    </div>
                    <div class="dataviz dataviz-tablet-mobile">
                      <?php the_sub_field('tablet_embed_code'); ?>
                    </div>
                  <?php elseif (empty(get_sub_field('tablet_embed_code')) && !empty(get_sub_field('mobile_embed_code'))) : ?>
                    <div class="dataviz dataviz-desktop-tablet">
                      <?php the_sub_field('desktop_embed_code'); ?>
                    </div>
                    <div class="dataviz dataviz-mobile">
                      <?php the_sub_field('mobile_embed_code'); ?>
                    </div>
                  <?php else : ?>
                    <div class="dataviz dataviz-desktop">
                      <?php the_sub_field('desktop_embed_code'); ?>
                    </div>
                    <div class="dataviz dataviz-tablet">
                      <?php the_sub_field('tablet_embed_code'); ?>
                    </div>
                    <div class="dataviz dataviz-mobile">
                      <?php the_sub_field('mobile_embed_code'); ?>
                    </div>
                  <?php endif; ?>
                  <?php if (!empty(get_sub_field('source'))) : ?>
                    <p class="wp-caption-text">
                      <?= strip_tags(get_sub_field('source'), '<a>'); ?>
                    </p>
                  <?php endif; ?>
                <?php elseif (get_row_layout() === 'table') : ?>
                  <?php $table = get_sub_field('table') ?>
                  <table class="table table-bordered <?php if (get_sub_field('striped_table')) : ?>table-striped<?php endif; ?>">
                    <?php if ($table['header']) : ?>
                      <?php $thead_color = get_sub_field('thead_color'); ?>
                      <?php if ($thead_color['colored_head']) : ?>
                        <?php $thead_class = 'thead-' . $thead_color['color']; ?>
                      <?php endif; ?>
                      <thead class="<?= $thead_class; ?>">
                        <tr>
                          <?php foreach ($table['header'] as $th) : ?>
                            <th><?= $th['c'] ?></th>
                          <?php endforeach; ?>
                        </tr>
                      </thead>
                    <?php endif; ?>
                    <tbody>
                      <?php foreach ($table['body'] as $tr) : ?>
                        <tr>
                          <?php foreach ($tr as $td) : ?>
                            <td><?= $td['c']; ?></td>
                          <?php endforeach; ?>
                        </tr>
                      <?php endforeach; ?>
                    </tbody>
                  </table>
                <?php endif; ?>
              <?php endwhile; ?>
            <?php else : ?>
              <?php the_content(); ?>
            <?php endif; ?>
          </div>
          <footer>
            <section class="entry-author-info">
              <?php $coauthors = get_coauthors(); ?>
              <?php foreach ($coauthors as $coauthor) : ?>
                <p class="author vcard">
                  <?= coauthors_posts_links_single($coauthor); ?>
                  <?php if (get_the_author_meta('description', $coauthor->ID)) : ?>
                    - <?= strip_tags(get_the_author_meta('description', $coauthor->ID)); ?>
                  <?php endif; ?>
                </p>
              <?php endforeach; ?>
            </section>
            <section class="row align-items-center d-print-none">
              <div class="col-12 col-md-7">
                <p>
                  Please send your feedback to <a href="#">respond@indiaspend.org</a>.
                </p>
                <p>
                  Liked this story? Indiaspend.org is a non-profit, and we depend on readers like you to drive our public-interest journalism efforts.
                </p>
              </div>
              <div class="col-12 col-md-5 text-md-center mb-3">
                <a href="#" class="btn btn-outline-primary">Donate</a>
                <a href="#" class="btn btn-outline-primary">Subscribe</a>
              </div>
            </section>
            <section class="post-category-list d-print-none">
              <?php the_category(' '); ?>
              <?php the_tags('', ' ', ''); ?>
            </section>
            <div class="alm-disqus" data-disqus-shortname="indiaspend"></div>
          </footer>
        </div>
        <?php if (Setup\display_sidebar()) : ?>
          <aside class="sidebar">
            <?php include Wrapper\sidebar_path(); ?>
          </aside><!-- /.sidebar -->
        <?php endif; ?>
      </div>
    </div>
  </article>
<?php else : ?>
  <article class="row mb-5">
    <div class="col-4 col-lg-7">
      <?php if (has_post_thumbnail()) : ?>
        <?php
          $id = get_post_thumbnail_id();
          $alt = get_post_meta($id, '_wp_attachment_image_alt', true);

          $featured_image_latest_src = wp_get_attachment_image_src($id, 'indiaspend-post-featured-image-latest');
          $featured_image_latest_srcset = wp_get_attachment_image_srcset($id, 'indiaspend-post-featured-image-latest');
          $featured_image_latest_small_src = wp_get_attachment_image_src($id, 'indiaspend-post-featured-image-small');
          $featured_image_latest_small_srcset = wp_get_attachment_image_srcset($id, 'indiaspend-post-featured-image-small');
        ?>
        <a href="<?php the_permalink(); ?>">
          <picture>
            <source media="(min-width: 992px)"
                    srcset="<?php echo esc_attr($featured_image_latest_srcset); ?>">
            <img class="img-fluid"
                 src="<?php echo esc_attr($featured_image_latest_small_src[0]); ?>"
                 srcset="<?php echo esc_attr($featured_image_latest_small_srcset); ?>"
                 sizes="(min-width: 768px) 240px,
                        (min-width: 576px) 180px,
                        191px"
                 alt="<?php echo esc_attr($alt);?>">
          </picture>
        </a>
      <?php endif; ?>
    </div>
    <header class="col-8 col-lg-5">
      <h3 class="entry-title" class="h4">
        <a href="<?php the_permalink(); ?>">
          <?php the_title(); ?>
        </a>
      </h3>
      <?php get_template_part('templates/entry-meta'); ?>
      <p class="d-none d-sm-block">
        <?= get_the_excerpt(); ?>
      </p>
    </header>
  </article>
<?php endif; ?>
