<?php
/**
 * Template Name: Team Template
 */
?>

<?php while (have_posts()) : the_post(); ?>
  <?php get_template_part('templates/content', 'page'); ?>
  <div class="container">
    <?php $members = get_field('members'); ?>
    <?php if ($members) : ?>
      <div class="row py-4">
        <?php foreach ($members as $member) : ?>
          <div class="col-6 col-md-3">
            <a href="#<?= 'user-' . $member['ID'] . '-modal' ?>" data-toggle="modal" role="button">
              <div class="text-center">
                <?php $member_image = get_field('image', 'user_' . $member['ID']) ?>
                <?php if ($member_image) : ?>
                  <img src="<?= $member_image['url'] ?>" alt="" class="rounded-circle img-fluid">
                <?php else : ?>
                  <?=
                    get_avatar(
                      $member['ID'],
                      512,
                      false,
                      get_the_author_meta('display_name'),
                      array( 'class' => 'rounded-circle img-fluid' )
                    );
                  ?>
                <?php endif; ?>
                <h2 class="h3 mt-3"><?= $member['user_firstname'] . ' ' . $member['user_lastname']; ?></h2>
                <h3 class="h4"><?= get_field('designation', 'user_' . $member['ID']) ?></h3>
                <?php $social_media_profiles = get_field('social_media_profiles', 'user_' . $member['ID']); ?>
                <ul class="list-inline">
                  <?php if ($social_media_profiles['twitter']) : ?>
                    <li class="list-inline-item">
                      <a href="<?= $social_media_profiles['twitter']; ?>" target="_blank">
                        <i class="fa fa-twitter" aria-hidden="true"></i>
                      </a>
                    </li>
                  <?php endif; ?>
                  <?php if ($social_media_profiles['linkedin']) : ?>
                    <li class="list-inline-item">
                      <a href="<?= $social_media_profiles['linkedin']; ?>" target="_blank">
                        <i class="fa fa-linkedin" aria-hidden="true"></i>
                      </a>
                    </li>
                  <?php endif; ?>
                  <li class="list-inline-item">
                    <a href="mailto:<?= $member['user_email']; ?>">
                      <i class="fa fa-envelope" aria-hidden="true"></i>
                    </a>
                  </li>
                </ul>
              </div>
            </a>
            <div class="modal fade" id="<?= 'user-' . $member['ID'] . '-modal'; ?>" tabindex="-1" role="dialog" aria-labelledby="<?= 'user-' . $member['ID'] . '-modal-label'; ?>" aria-hidden="true">
              <div class="modal-dialog" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                  </div>
                  <div class="modal-body">
                    <article>
                      <header class="text-center">
                        <?php $member_image = get_field('image', 'user_' . $member['ID']); ?>
                        <?php if ($member_image) : ?>
                          <img src="<?= $member_image['url']; ?>" alt="<?= $member['user_firstname'] . ' ' . $member['user_lastname']; ?>" class="rounded-circle img-fluid d-block mx-auto">
                        <?php else : ?>
                          <?=
                            get_avatar(
                              $member['ID'],
                              120,
                              false,
                              get_the_author_meta('display_name'),
                              array( 'class' => 'rounded-circle img-fluid d-block mx-auto' )
                            );
                          ?>
                        <?php endif; ?>
                        <h2 class="modal-title h3 font-weight-bold" id="<?= 'user-' . $member['ID'] . '-modal-label' ?>"><?= $member['user_firstname'] . ' ' . $member['user_lastname']; ?></h3>
                        <h3 class="h3"><?= get_field('designation', 'user_' . $member['ID']); ?></h3>
                        <ul class="list-inline">
                          <?php if ($social_media_profiles['twitter']) : ?>
                            <li class="list-inline-item">
                              <a href="<?= $social_media_profiles['twitter']; ?>" target="_blank">
                                <i class="fa fa-twitter" aria-hidden="true"></i>
                              </a>
                            </li>
                          <?php endif; ?>
                          <?php if ($social_media_profiles['linkedin']) : ?>
                            <li class="list-inline-item">
                              <a href="<?= $social_media_profiles['linkedin']; ?>" target="_blank">
                                <i class="fa fa-linkedin" aria-hidden="true"></i>
                              </a>
                            </li>
                          <?php endif; ?>
                          <li class="list-inline-item">
                            <a href="mailto:<?= $member['user_email']; ?>">
                              <i class="fa fa-envelope" aria-hidden="true"></i>
                            </a>
                          </li>
                        </ul>
                      </header>
                      <div class="text-justify">
                        <p>
                          <?= $member['user_description']; ?>
                        </p>
                      </div>
                    </article>
                  </div>
                </div>
              </div>
            </div>
          </div>
        <?php endforeach; ?>
      </div>
    <?php endif; ?>
  </div>
<?php endwhile; ?>
