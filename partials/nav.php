<nav class="navbar navbar-expand-lg navbar-light secondary-navigation">
  <span class="secondary-navigation-time"><?= date('D, jS M, Y'); ?></span>
  <?php
  if (has_nav_menu('secondary_navigation')) :
    wp_nav_menu([
      'theme_location'  => 'secondary_navigation',
      'menu_class'      => 'navbar-nav',
      'depth'           => 2,
      'container'       => 'div',
      'container_class' => 'collapse navbar-collapse justify-content-end',
      'fallback_cb'     => 'WP_Bootstrap_Navwalker::fallback',
      'walker'          => new WP_Bootstrap_Navwalker()
    ]);
  endif;
  ?>
</nav>
<nav class="navbar navbar-expand-lg navbar-light primary-navigation">
  <button class="navbar-toggler d-print-none" type="button" data-toggle="collapse" data-target="#primaryNavbarContent" aria-controls="primaryNavbarContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <a class="navbar-brand order-lg-1" href="<?= esc_url(home_url('/')); ?>">
    <img src="<?= get_stylesheet_directory_uri(); ?>/assets/images/logo.png" alt="<?php bloginfo('name'); ?>" width="150">
  </a>
  <div class="order-lg-3">
    <a href="/search" class="fa fa-search nav-link pr-2 d-print-none" aria-hidden="true"></a>
  </div>
  <?php
  if (has_nav_menu('secondary_navigation')) :
    $secondary_navigation = wp_nav_menu([
      'theme_location'  => 'secondary_navigation',
      'depth'           => 2,
      'container'       => '',
      'fallback_cb'     => 'WP_Bootstrap_Navwalker::fallback',
      'items_wrap'      => '<div class="dropdown-divider"></div><ul id="menu-main-menu" class="navbar-nav d-lg-none">%3$s</ul>',
      'walker'          => new WP_Bootstrap_Navwalker(),
      'echo'            => false
    ]);
  endif;
  ?>
  <?php
  if (has_nav_menu('primary_navigation')) :
    wp_nav_menu([
      'theme_location'  => 'primary_navigation',
      'menu_class'      => 'navbar-nav',
      'depth'           => 2,
      'container'       => 'div',
      'container_class' => 'collapse navbar-collapse justify-content-end order-lg-2',
      'container_id'    => 'primaryNavbarContent',
      'items_wrap'      => '<ul id="menu-main-menu" class="navbar-nav">%3$s'. $secondary_navigation . '</ul>',
      'fallback_cb'     => 'WP_Bootstrap_Navwalker::fallback',
      'walker'          => new WP_Bootstrap_Navwalker()
    ]);
  endif;
  ?>
</nav>
