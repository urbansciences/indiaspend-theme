<?php
$related = Jetpack_RelatedPosts::init_raw()
  ->set_query_name('jetpack-related-shortcode')
  ->get_for_post_id(
    get_the_ID(),
    array( 'size' => 3 )
  );
?>

<?php if ($related) : ?>
  <?php global $post; ?>
  <?php foreach ($related as $result) : ?>
    <?php setup_postdata($result); ?>
    <?php $related_post = get_post($result['id']); ?>
    <article class="post-large">
      <header>
        <?php if (has_post_thumbnail()) : ?>
          <?php
            $id = get_post_thumbnail_id();
            $alt = get_post_meta($id, '_wp_attachment_image_alt', true);

            $featured_image_large_src = wp_get_attachment_image_src($id, 'indiaspend-post-featured-image-large');
            $featured_image_large_srcset = wp_get_attachment_image_srcset($id, 'indiaspend-post-featured-image-large');
          ?>
          <a href="<?php the_permalink(); ?>">
            <img src="<?php echo esc_attr($featured_image_large_src[0]); ?>"
                srcset="<?php echo esc_attr($featured_image_large_srcset); ?>"
                sizes="(min-width: 1200px) 540px,
                       (min-width: 992px) 450px,
                       (min-width: 768px) 330px,
                       (min-width: 576px) 510px,
                       545px"
                alt="<?php echo esc_attr($alt);?>"
                class="img-fluid">
          </a>
        <?php endif; ?>
        <h3 class="entry-title">
          <a href="<?php the_permalink(); ?>">
            <?php the_title(); ?>
          </a>
        </h3>
        <div class="entry-info">
          <address class="vcard subtitle">
            <?php coauthors_posts_links(null, ', ', null, null, true); ?>
          </address>
          <time class="text-muted subtitle updated d-block mb-3" datetime="<?= get_post_time('c', true); ?>"><?= get_the_date(); ?></time>
        </div>
      </header>
    </article>
  <?php endforeach; ?>
  <?php wp_reset_postdata(); ?>
<?php endif; ?>
