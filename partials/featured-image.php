<?php if (has_post_thumbnail()) : ?>
  <?php
    $id = get_post_thumbnail_id();
    $alt = get_post_meta($id, '_wp_attachment_image_alt', true);

    $featured_image_desktop_src = wp_get_attachment_image_src($id, 'indiaspend-post-featured-image-desktop');

    $featured_image_mobile_src = wp_get_attachment_image_src($id, 'indiaspend-post-featured-image-mobile');
  ?>
  <figure class="figure mt-2 mb-5">
    <picture>
      <source media="(min-width: 768px)"
              srcset="<?php echo esc_attr($featured_image_desktop_src[0]); ?> 1440w"
              class="figure-img img-fluid">
      <img class="d-block w-100 figure-img img-fluid" src="<?php echo esc_attr($featured_image_mobile_src[0]); ?>"
          srcset="<?php echo esc_attr($featured_image_mobile_src[0]); ?> 768w"
          alt="<?php echo esc_attr($alt);?>"
          sizes="100vw">
    </picture>
    <figcaption class="figure-caption container subtitle mt-2">
      <?php the_post_thumbnail_caption(); ?>
    </figcaption>
  </figure>
<?php endif; ?>
