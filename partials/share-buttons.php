<nav class="col-lg-1">
  <ul class="sharing-buttons sharing-buttons--desktop list-unstyled">
    <li class="facebook-share-icon">
      <a href="https://www.facebook.com/sharer/sharer.php?u=<?= get_permalink(); ?>" target="_blank"><i class="fa fa-facebook" aria-hidden="true"></i></a>
    </li>
    <li class="twitter-share-icon">
      <a href="https://twitter.com/home?status=<?= get_permalink(); ?>" target="_blank"><i class="fa fa-twitter" aria-hidden="true"></i></a>
    </li>
    <li class="linkedin-share-icon">
      <a href="https://www.linkedin.com/cws/share?url=<?= get_permalink(); ?>" target="_blank"><i class="fa fa-linkedin" aria-hidden="true"></i></a>
    </li>
    <li class="google-plus-share-icon">
      <a href="https://plus.google.com/share?url=<?= get_permalink(); ?>" target="_blank"><i class="fa fa-google-plus" aria-hidden="true"></i></a>
    </li>
    <li class="envelope-share-icon">
      <a href="mailto:?body=<?= get_permalink(); ?>"><i class="fa fa-envelope" aria-hidden="true"></i></a>
    </li>
    <li class="print-share-icon">
      <a href="javascript:window.print()"><i class="fa fa-print" aria-hidden="true"></i></a>
    </li>
  </ul>
</nav>
<div class="text-center mobile-share-button d-block d-lg-none d-print-none">
  <i class="fa fa-share" aria-hidden="true"></i>
</div>
