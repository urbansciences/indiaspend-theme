<?php

use Roots\Sage\Setup;
use Roots\Sage\Wrapper;

?>

<!doctype html>
<html <?php language_attributes(); ?>>
  <?php get_template_part('templates/head'); ?>
  <body <?php body_class('regular-grid'); ?>>
    <!--[if IE]>
      <div class="alert alert-warning">
        <?php _e('You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.', 'sage'); ?>
      </div>
    <![endif]-->
    <?php
    do_action('get_header');
    if (is_archive()) {
      if (is_category(Setup\get_main_categories())) {
        get_template_part('templates/header');
      } else {
        get_template_part('templates/header', 'archive');
      }
    } elseif (is_page_template()) {
      get_template_part('templates/header', 'page');
    } elseif (is_singular('page')) {
      get_template_part('templates/header', 'page');
    } elseif (is_search()) {
      get_template_part('templates/header', 'search');
    } else {
      get_template_part('templates/header');
    }
    ?>
    <?php if (is_category(Setup\get_main_categories()) || is_404()) : ?>
    <div class="wrap" role="document">
      <div class="content row no-gutters">
    <?php else : ?>
    <div class="wrap container" role="document">
      <div class="content row">
    <?php endif; ?>
        <main class="main">
          <?php include Wrapper\template_path(); ?>
        </main><!-- /.main -->
        <?php if (Setup\display_sidebar()) : ?>
          <aside class="sidebar">
            <?php include Wrapper\sidebar_path(); ?>
          </aside><!-- /.sidebar -->
        <?php endif; ?>
      </div><!-- /.content -->
    </div><!-- /.wrap -->
    <?php
      do_action('get_footer');
      get_template_part('templates/footer');
      wp_footer();
    ?>
  </body>
</html>
