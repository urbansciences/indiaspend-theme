<?php
/**
 * Template Name: Donate Template
 */
?>

<?php while (have_posts()) : the_post(); ?>
  <div class="container py-4">
    <div class="row justify-content-center">
      <div class="col-12 col-md-8 text-center">
        <?php get_template_part('templates/content', 'page'); ?>
      </div>
      <div class="w-100"></div>
      <form class="col-12 col-md-6 py-4">
        <div class="form-group">
          <label for="donationName">Name:</label>
          <input type="text" class="form-control" id="donationName" required>
        </div>
        <div class="form-group">
          <label for="donationEmail">Email:</label>
          <input type="email" class="form-control" id="donationEmail" required>
        </div>
        <div class="form-group">
          <label for="donationPhone">Phone Number:</label>
          <input type="tel" class="form-control" id="donationPhone" required>
        </div>
        <fieldset>
          <legend class="col-form-legend">Amount (minimum ₹ 250):</legend>
          <div class="form-check">
            <label class="form-check-label">
              <input class="form-check-input" type="radio" name="donationAmount" id="amountContributor" value="amountContributor" checked>
              ₹ 250 - Contributor
            </label>
          </div>
          <div class="form-check">
            <label class="form-check-label">
              <input class="form-check-input" type="radio" name="donationAmount" id="amountSupporter" value="amountSupporter">
              ₹ 1,000 - Supporter
            </label>
          </div>
          <div class="form-check">
            <label class="form-check-label">
              <input class="form-check-input" type="radio" name="donationAmount" id="amountBenefactor" value="amountBenefactor">
              ₹ 3,000 - Benefactor
            </label>
          </div>
          <div class="form-check">
            <label class="form-check-label">
              <input class="form-check-input" type="radio" name="donationAmount" id="donationPatron" value="donationPatron">
              ₹ 5,000 - Patron
            </label>
          </div>
          <div class="form-check">
            <label class="form-check-label">
              <input class="form-check-input" type="radio" name="donationAmount" id="donationCustom" value="donationCustom">
              <input type="number" class="form-control">
            </label>
          </div>
        </fieldset>
        <div class="form-group text-center py-4">
          <button type="submit" class="btn btn-outline-primary">Proceed to Payment</button>
        </div>
      </form>
    </div>
  </div>
<?php endwhile; ?>
