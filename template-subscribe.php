<?php
/**
 * Template Name: Subscribe Template
 */
?>

<?php while (have_posts()) : the_post(); ?>
  <div class="container py-4">
    <div class="row">
      <div class="col-12 col-md-6 offset-md-3">
        <?php get_template_part('templates/content', 'page'); ?>
        <!-- Begin MailChimp Signup Form -->
        <div id="mc_embed_signup">
          <form action="https://indiaspend.us2.list-manage.com/subscribe/post?u=f7df8bde5e23a35e91375835f&amp;id=33140735d2" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
            <div id="mc_embed_signup_scroll">
              <div class="indicates-required"><span class="asterisk">*</span> indicates required</div>
              <div class="mc-field-group form-group row">
                <label for="mce-FNAME" class="col-sm-4 col-form-label">First Name  <span class="asterisk">*</span></label>
                <div class="col-sm-8">
                  <input type="text" value="" name="FNAME" class="required form-control" id="mce-FNAME">
                </div>
              </div>
              <div class="mc-field-group form-group row">
                <label for="mce-LNAME" class="col-sm-4 col-form-label">Last Name  <span class="asterisk">*</span></label>
                <div class="col-sm-8">
                  <input type="text" value="" name="LNAME" class="required form-control" id="mce-LNAME">
                </div>
              </div>
              <div class="mc-field-group form-group row">
                <label for="mce-EMAIL" class="col-sm-4 col-form-label">Email Address  <span class="asterisk">*</span></label>
                <div class="col-sm-8">
                  <input type="email" value="" name="EMAIL" class="required email form-control" id="mce-EMAIL">
                </div>
              </div>
              <div id="mce-responses" class="clear">
                <div class="response" id="mce-error-response" style="display:none"></div>
                <div class="response" id="mce-success-response" style="display:none"></div>
              </div>    <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
              <div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_f7df8bde5e23a35e91375835f_33140735d2" tabindex="-1" value=""></div>
              <div class="clear text-center">
                <input type="submit" value="Subscribe" name="subscribe" id="mc-embedded-subscribe" class="button btn btn-outline-primary">
              </div>
              <p>
                <a href="https://us2.campaign-archive.com/home/?u=f7df8bde5e23a35e91375835f&id=33140735d2" title="View previous campaigns" target="_blank">View previous campaigns.</a>
              </p>
            </div>
          </form>
        </div>
        <!--End mc_embed_signup-->
      </div>
    </div>
  </div>
<?php endwhile; ?>
