<?php
$do_not_duplicate = [];

$sections = get_field('sections');

if ($sections) {
  foreach ($sections as $section) {
    if ($section['acf_fc_layout'] === 'carousel') {
      get_template_part('templates/home', 'featured-posts');
    } elseif ($section['acf_fc_layout'] === 'top_sections') {
      $top_section_categories = $section['categories'];
      get_template_part('templates/home', 'top-sections');
    } elseif ($section['acf_fc_layout'] === 'donate_section') {
      get_template_part('templates/home', 'donate');
    } elseif ($section['acf_fc_layout'] === 'featured_data_viz') {
      get_template_part('templates/home', 'special');
    } elseif ($section['acf_fc_layout'] === 'latest_stories') {
      get_template_part('templates/home', 'latest');
    }
  }
}
